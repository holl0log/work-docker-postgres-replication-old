#!/bin/bash
#
# Recovers WAL file $1 from one of the HOSTS, restoring it to $2. The archive path should be parameter 3, and the hosts in the cluster are the parameters after that.
#
LOG_FACILITY="local0"

SCRIPT_NAME=`basename $0`
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "find_wal_log_for_recovery $@"
echo "find_wal_log_for_recovery $@"

WAL="$1"
shift
TARGET="$1"
shift
ARCHIVE_PATH="$1"
shift

logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "WAL: $WAL"
echo "WAL: $WAL"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "TARGET: $TARGET"
echo "TARGET: $TARGET"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "ARCHIVE_PATH: $ARCHIVE_PATH"
echo "ARCHIVE_PATH: $ARCHIVE_PATH"

LOCALHOST=`uname -n`
REMOTE_HOSTS=()

while [ $# -gt 0 ]; do
        if [[ "$1" != "$LOCALHOST" ]]; then
                REMOTE_HOSTS=("${REMOTE_HOSTS[@]}" "$1")
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is a remote host"
				        echo "$1 is a remote host"
        else
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is the local host"
				        echo "$1 is the local host"
        fi
        shift
done

# Now that all the parameters are parsed, let's check the local path and retrieve from a remote if we need to.

# If we don't have the file in the local archive, try to grab the file from the remote hosts.
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "Looking for local archive file $ARCHIVE_PATH/$WAL"
echo "Looking for local archive file $ARCHIVE_PATH/$WAL"
ls "$ARCHIVE_PATH/$WAL" 2>/dev/null >/dev/null
if [[ $? != "0" ]]; then
        logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "Missing local archive. Checking Remote Hosts..."
		    echo "Missing local archive. Checking Remote Hosts..."

        recovered="false"
        for REMOTE in "${REMOTE_HOSTS[@]}"; do
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "Attempting to recover archive from $REMOTE:$ARCHIVE_PATH/$WAL"
				        echo "Attempting to recover archive from $REMOTE:$ARCHIVE_PATH/$WAL"
                # rsync -a "$REMOTE:$ARCHIVE_PATH/$WAL" "$ARCHIVE_PATH/$WAL" </dev/null
                rsync -a "/$REMOTE$ARCHIVE_PATH/$WAL" "$ARCHIVE_PATH/$WAL" </dev/null
				        # cp "/$REMOTE$ARCHIVE_PATH/$WAL" "$ARCHIVE_PATH/$WAL"
                ret=$?
                if [[ "$ret" == "0" ]]; then
                        logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "$LOCALHOST recovered '$ARCHIVE_PATH/$WAL' to local archive from $REMOTE"
						            echo "$LOCALHOST recovered '$ARCHIVE_PATH/$WAL' to local archive from $REMOTE"
                        recovered="true"
                        break;
                fi
        done

        if [[ "$recovered" != "true" ]]; then
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.warning "$LOCALHOST cannot recover '$ARCHIVE_PATH/$WAL' from any remote hosts."
				        echo "$LOCALHOST cannot recover '$ARCHIVE_PATH/$WAL' from any remote hosts."
                exit 1
        fi
else
        logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "Local archive exists."
        echo "Local archive exists."
fi

# Now, try to copy file into the proper recovery location
cp "$ARCHIVE_PATH/$WAL" "$TARGET"
