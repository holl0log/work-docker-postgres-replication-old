#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
CREATE USER onlinestore WITH
  ENCRYPTED PASSWORD 'onlinestore'
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  VALID UNTIL 'infinity';

CREATE USER shippinguser WITH
  ENCRYPTED PASSWORD 'shippinguser'
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  VALID UNTIL 'infinity';

CREATE USER replication WITH
  ENCRYPTED PASSWORD 'replication'
  LOGIN
  NOSUPERUSER
  INHERIT
  CREATEDB
  CREATEROLE
  REPLICATION
  VALID UNTIL 'infinity';

CREATE DATABASE onlinestore WITH
  OWNER = onlinestore
  ENCODING = 'UTF8'
  LC_COLLATE = 'en_US.utf8'
  LC_CTYPE = 'en_US.utf8'
  TABLESPACE = pg_default
  CONNECTION LIMIT = -1;

GRANT CONNECT ON DATABASE onlinestore TO shippinguser;
GRANT TEMPORARY, CONNECT ON DATABASE onlinestore TO PUBLIC;
GRANT ALL ON DATABASE onlinestore TO onlinestore;

EOSQL


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname onlinestore <<-EOSQL
CREATE SCHEMA shipping AUTHORIZATION onlinestore;
GRANT ALL ON SCHEMA shipping TO onlinestore;
ALTER USER shippinguser SET search_path TO shippinguser, shipping, public;
GRANT USAGE ON SCHEMA shipping TO shippinguser;

CREATE SEQUENCE shipping.carrier_details_id_seq;
ALTER SEQUENCE shipping.carrier_details_id_seq OWNER TO onlinestore;
GRANT SELECT, USAGE ON SEQUENCE shipping.carrier_details_id_seq TO shippinguser;
GRANT ALL ON SEQUENCE shipping.carrier_details_id_seq TO onlinestore;

CREATE SEQUENCE shipping.packslips_id_seq;
ALTER SEQUENCE shipping.packslips_id_seq OWNER TO onlinestore;
GRANT SELECT, USAGE ON SEQUENCE shipping.packslips_id_seq TO shippinguser;
GRANT ALL ON SEQUENCE shipping.packslips_id_seq TO onlinestore;

CREATE TABLE shipping.carrier_details
(
    id integer NOT NULL DEFAULT nextval('shipping.carrier_details_id_seq'::regclass),
    order_number_int integer,
  order_number_num numeric(6,0),
  order_number_bigint bigint,
  order_number_char char(6),
  order_number_varchar character varying(255),
    daily_rate numeric(8,2),
    tracking_number character varying(255) COLLATE pg_catalog."default",
    created_at timestamp with time zone DEFAULT now(),
    CONSTRAINT carrier_details_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE shipping.carrier_details OWNER to onlinestore;
GRANT INSERT, SELECT, UPDATE ON TABLE shipping.carrier_details TO shippinguser;
GRANT ALL ON TABLE shipping.carrier_details TO onlinestore;

CREATE TABLE shipping.packslips
(
    id integer NOT NULL DEFAULT nextval('shipping.packslips_id_seq'::regclass),
    order_number integer NOT NULL,
    shipping_carrier character varying(255) COLLATE pg_catalog."default",
    shipping_method character varying(255) COLLATE pg_catalog."default",
    carrier_account_number character varying(255) COLLATE pg_catalog."default",
    company_name character varying(255) COLLATE pg_catalog."default",
    customer_name character varying(255) COLLATE pg_catalog."default",
    address_1 character varying(255) COLLATE pg_catalog."default",
    address_2 character varying(255) COLLATE pg_catalog."default",
    city character varying(255) COLLATE pg_catalog."default",
    state character varying(255) COLLATE pg_catalog."default",
    zip character varying(255) COLLATE pg_catalog."default",
    phone character varying(255) COLLATE pg_catalog."default",
    email_1 character varying(255) COLLATE pg_catalog."default",
    email_1_present boolean,
    email_2 character varying(255) COLLATE pg_catalog."default",
    email_2_present boolean,
    email_3 character varying(255) COLLATE pg_catalog."default",
    email_3_present boolean,
    comments text COLLATE pg_catalog."default",
    ticket_number character varying(255) COLLATE pg_catalog."default",
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone NOT NULL DEFAULT now(),
    purchase_order_number character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT packslips_pkey PRIMARY KEY (id),
    CONSTRAINT packslips_order_number_unique UNIQUE (order_number)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE shipping.packslips OWNER to onlinestore;
GRANT SELECT ON TABLE shipping.packslips TO shippinguser;
GRANT ALL ON TABLE shipping.packslips TO onlinestore;


INSERT INTO shipping.packslips (order_number,shipping_carrier,shipping_method,carrier_account_number,company_name,customer_name,address_1,address_2,city,state,zip,phone,email_1,email_1_present,email_2,email_2_present,email_3,email_3_present,comments,ticket_number,purchase_order_number)
  VALUES (100005,'DHL','Express Worldwide','None','MAGNETI MARELLI SUS S PLANT 33','SZYMON DORDA','GRAZYNSKIEGO 141','PLANT 33, HALA 3D','Bielsko-Biala','Woj. Śląskie',43-300,'+48 571 294 050','jdebshaw@polarislabs.com',TRUE,'mdesjardins@polarislabs.com',TRUE,'mdesjardins@polarislabs.com',TRUE,'put happy stickers on the end of box','ticketnum','ponum');

INSERT INTO shipping.carrier_details (order_number_int,order_number_num,order_number_bigint,order_number_char,order_number_varchar,daily_rate,tracking_number)
  VALUES (200000,200000,200000,200000,200000,16.59,'1Z02E14W0360729353'),
  (200001,200001,200001,200001,200001,13.07,'1Z02E14W0358775763'),
  (200002,200002,200002,200002,200002,28.29,'1Z02E14W0360520621');

EOSQL
