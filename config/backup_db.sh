#!/bin/bash
#
# Snapshot (rsync) a base backup to an archive server for DR recovery.
#
LOG_FACILITY="local2"
SCRIPT_NAME=`basename $0`

logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$SCRIPT_NAME $@"
echo "$SCRIPT_NAME $@"

DB_PORT="$1"
shift
PG_DATA="$1"
shift
ARCHIVE_DIR="$1"
shift
ARCHIVE_HOST="$1"
shift

logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "DB_PORT: $DB_PORT"
echo "DB_PORT: $DB_PORT"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "PG_DATA: $PG_DATA"
echo "PG_DATA: $PG_DATA"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "ARCHIVE_DIR: $ARCHIVE_DIR"
echo "ARCHIVE_DIR: $ARCHIVE_DIR"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "ARCHIVE_HOST: $ARCHIVE_HOST"
echo "ARCHIVE_HOST: $ARCHIVE_HOST"


LOCALHOST=`uname -n`
REMOTE_HOSTS=()
# Populate the REMOTE_HOSTS array.
while [ $# -gt 0 ]; do
	if [[ "$1" != "$LOCALHOST" ]]; then
		REMOTE_HOSTS=("${REMOTE_HOSTS[@]}" "$1")
		logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is a remote host"
    echo "$1 is a remote host"
	else
		logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is the local host"
    echo "$1 is the local host"
	fi
	shift
done
# The Archive host counts as a REMOTE_HOST
REMOTE_HOSTS=("${REMOTE_HOSTS[@]}" "$ARCHIVE_HOST")

BACKUP_LABEL="nightly_backup:$(date --rfc-3339=seconds)"

# Make sure we are the master server.
pgrep -f "postgres: wal writer process" >/dev/null
if [ $? = 0 ]; then
	recovMode=$(psql -p $DB_PORT -At -c "select pg_is_in_recovery()")
	if [ "$?" -ne "0" ]; then
		logger -t $SCRIPT_NAME -p $LOG_FACILITY.emerg "Error querying local Postgres instance recovery state"
    echo "Error querying local Postgres instance recovery state"
		exit 1
	fi
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "pg_is_in_recovery(): $recovMode"
  echo "pg_is_in_recovery(): $recovMode"
	if [ "f" != "$recovMode" ]; then
		logger -t $SCRIPT_NAME -p $LOG_FACILITY.warning "Local PostgreSQL instance pg_is_in_recovery() returned $recovMode"
    echo "Local PostgreSQL instance pg_is_in_recovery() returned $recovMode"
		exit 1
	fi
else
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "PostgreSQL instance is standby, skipping $SCRIPT_NAME."
  echo "PostgreSQL instance is standby, skipping $SCRIPT_NAME."
	exit 0
fi

# Signal postgresql into backup mode and label the backup.
started=$(date +"%s")
logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Beginning backup labeled '$BACKUP_LABEL'"
echo "Beginning backup labeled '$BACKUP_LABEL'"
psql -p $DB_PORT -At -c "SELECT pg_start_backup('$BACKUP_LABEL', true);"
if [ "$?" -ne "0" ]; then
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.emerg "Error invoking pg_start_backup()"
  echo "Error invoking pg_start_backup()"
	exit 1
fi

# Rsync to the archive server.
# uncomment this section in prod, comment out this section in qa
SYNC_SIZE=$(du -hs --exclude=$PG_DATA/pg_xlog $PG_DATA | awk '{print $1}')
logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "rsync'ing $SYNC_SIZE from $PG_DATA to $ARCHIVE_HOST:$PG_DATA"
echo "rsync'ing $SYNC_SIZE from $PG_DATA to $ARCHIVE_HOST:$PG_DATA"
# rsync -av --delete --exclude "pg_xlog/*" -e "ssh -c arcfour" $PG_DATA/ $ARCHIVE_HOST:$PG_DATA 2>&1 | logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug
rsync -av --delete --exclude "pg_xlog/*" $PG_DATA/ /$ARCHIVE_HOST$PG_DATA 2>&1
if [ "$?" -ne "0" ]; then
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.emerg "PostgreSQL backup (rsync) FAILED for $PG_DATA to $ARCHIVE_HOST"
  echo "PostgreSQL backup (rsync) FAILED for $PG_DATA to $ARCHIVE_HOST"
fi

# Signal postgresql out of backup mode.
cp $PG_DATA/backup_label $PG_DATA/backup_label.old
psql -p $DB_PORT -At -c "SELECT pg_stop_backup();"
if [ "$?" -ne "0" ]; then
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.emerg "Error invoking pg_stop_backup!"
  echo "Error invoking pg_stop_backup!"
	exit 1
fi

stopped=$(date +"%s")
elapsed=$(($stopped - $started))
logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Archive baseline backup rsync completed in $elapsed seconds"
echo "Archive baseline backup rsync completed in $elapsed seconds"


# Prune old WAL logs & Backup markers.
started=$(date +"%s")
keep_prefix=$(cat $PG_DATA/backup_label.old | grep "START WAL LOCATION" | sed -E 's/^.*file (.*?)\)$/\1/')
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "Grooming archived WAL. Keeping prefix: $keep_prefix"
echo "Grooming archived WAL. Keeping prefix: $keep_prefix"

# Local archive cleanup.
for f in `ls $ARCHIVE_DIR`; do
	#Skip any .hisotry files..
	if [[ $f =~ \.history ]]; then
		continue
	fi

	prefix=$(echo $f | awk -F . '{print $1}')
	res=$(echo "ibase=16; $prefix < $keep_prefix"| bc)
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$f prefix: $prefix res: $res"
  echo "$f prefix: $prefix res: $res"
	if [ "$res" = 1 ]; then
		logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Deleting outdated archive file $f"
    echo "Deleting outdated archive file $f"
		rm -f $ARCHIVE_DIR/$f
	fi
done

# Remote archives cleanup.
ret="-1"
for REMOTE in "${REMOTE_HOSTS[@]}"; do
	logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "Getting remote file listing on $REMOTE"
  echo "Getting remote file listing on $REMOTE"
	# for f in $(ssh $REMOTE "ls $ARCHIVE_DIR"); do
  for f in $(ls /$REMOTE$ARCHIVE_DIR); do
		#Skip any .history files
		if [[ $f =~ \.history ]]; then
			continue
		fi

		prefix=$(echo $f | awk -F . '{print $1}')
		res=$(echo "ibase=16; $prefix < $keep_prefix"| bc)
		if [ "$res" = 1 ]; then
			logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Deleting outdated archive file $REMOTE:$f"
      echo "Deleting outdated archive file $REMOTE:$f"
			# rsh -q -o PasswordAuthentication=no "$REMOTE" "rm -f $ARCHIVE_DIR/$f"
      rm -f "/$REMOTE$ARCHIVE_DIR/$f"
		fi
	done
done

stopped=$(date +"%s")
elapsed=$(($stopped - $started))
logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "WAL Archive grooming completed in $elapsed seconds"
echo "WAL Archive grooming completed in $elapsed seconds"
