ARG PGVERSION

FROM postgres:${PGVERSION}
ENV PGARCHIVE /var/lib/postgresql/archive
RUN mkdir -p "$PGARCHIVE" && chown -R postgres:postgres "$PGARCHIVE" && chmod 777 "$PGARCHIVE"
RUN apt-get update && apt-get install -y --no-install-recommends rsync bc && rm -rf /var/lib/apt/lists/*
VOLUME /var/lib/postgresql/archive
