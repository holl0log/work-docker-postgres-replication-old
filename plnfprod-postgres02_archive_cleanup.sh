#!/bin/bash
#
# Cleans up WAL archive files from postgres nodes.
#
LOG_FACILITY="local0"

SCRIPT_NAME=`basename $0`
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$SCRIPT_NAME $@"

LOCALHOST=`uname -n`
REMOTE_HOSTS=()

ARCHIVE_PATH=$1
shift
KEEPER=$1
shift

logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "ARCHIVE_PATH: $ARCHIVE_PATH"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "KEEPER: $KEEPER"

while [ $# -gt 0 ]; do
        if [[ "$1" != "$LOCALHOST" ]]; then
                REMOTE_HOSTS=("${REMOTE_HOSTS[@]}" "$1")
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is a remote host"
        else
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is the local host"
        fi
        shift
done

# On each machine invoke pg_archivecleanup with the proper archive path and KEEPER
ret="-1"
for REMOTE in "${REMOTE_HOSTS[@]}"; do
        logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Cleaning up archive '$REMOTE:$ARCHIVE_PATH' up to $KEEPER"
        rsh -q -o PasswordAuthentication=no "$REMOTE" "/usr/lib/postgresql/9.3/bin/pg_archivecleanup -d $ARCHIVE_PATH $KEEPER" 2>&1 | logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug
        ret=$?
        if [[ "$ret" == "0" ]]; then
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Cleaned up archive directory '$REMOTE:$ARCHIVE_PATH/$WAL'"
        fi
done

logger -t $SCRIPT_NAME -p $LOG_FACILITY.info "Cleaning up archive '$LOCALHOST:$ARCHIVE_PATH' up to $KEEPER"
/usr/lib/postgresql/9.3/bin/pg_archivecleanup -d $ARCHIVE_PATH $KEEPER 2>&1 | logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug
