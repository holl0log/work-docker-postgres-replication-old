#!/bin/bash
#
# Copies an archive file to the archive directory locally, and then rsynchs them to any remotes it can.
#
LOG_FACILITY="local0"

SCRIPT_NAME=`basename $0`
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$SCRIPT_NAME $@"

SOURCE="$1"
shift
WAL="$1"
shift
ARCHIVE_PATH="$1"
shift
ARCHIVE_HOST="$1"
shift

logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "SOURCE: $SOURCE"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "WAL: $WAL"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "ARCHIVE_PATH: $ARCHIVE_PATH"
logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "ARCHIVE_HOST: $ARCHIVE_HOST"

LOCALHOST=`uname -n`
REMOTE_HOSTS=("$ARCHIVE_HOST")

while [ $# -gt 0 ]; do
        if [[ "$1" != "$LOCALHOST" ]]; then
                REMOTE_HOSTS=("${REMOTE_HOSTS[@]}" "$1")
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is a remote host"
        else
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.debug "$1 is the local host"
        fi
        shift
done

# Now that all the parameters are parsed, copy to the local, and then to the remotes.
cp "$SOURCE" "$ARCHIVE_PATH/$WAL"
ret=$?
if [[ "$ret" != "0" ]]; then
        logger -t $SCRIPT_NAME -p $LOG_FACILITY.emerg "$LOCALHOST failed to locally archive $WAL."
        exit 1
fi

for REMOTE in "${REMOTE_HOSTS[@]}"; do
        rsync -a "$SOURCE" "$REMOTE:$ARCHIVE_PATH/" </dev/null
        ret=$?
        if [[ "$ret" != "0" ]]; then
                logger -t $SCRIPT_NAME -p $LOG_FACILITY.emerg "$LOCALHOST failed to remotely archive $WAL to '$REMOTE:$ARCHIVE_PATH/$WAL'"
                rm "$ARCHIVE_PATH/$WAL"
                exit 1
        fi
done

# If we make it this far, we're good.
exit 0
